interface IConfig {
  graphqlUri: string
}

export const config: IConfig = {
  graphqlUri: process.env.GRAPHQL_URI,
}
