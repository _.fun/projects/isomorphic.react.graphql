import React from 'react'
import { ApolloProvider } from 'react-apollo'
import { StaticRouter } from 'react-router'
import { ThemeProvider } from 'styled-components'

import { App } from '@/app'

interface IRootProps {
  client: any
  theme: any
  context: any
  location: any
}

export const Root: React.FunctionComponent<IRootProps> = ({
  client,
  theme,
  context,
  location,
}) => (
  <ApolloProvider client={client}>
    <ThemeProvider theme={theme}>
      <StaticRouter location={location} context={context}>
        <App />
      </StaticRouter>
    </ThemeProvider>
  </ApolloProvider>
)
