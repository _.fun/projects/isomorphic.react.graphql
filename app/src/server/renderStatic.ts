import { NormalizedCacheObject } from 'apollo-cache-inmemory'

interface IRenderStatic {
  assets: any
  styleTags: string
  markup: string
  initialApolloState: NormalizedCacheObject
}

export const renderStatic = ({
  assets,
  styleTags,
  markup,
  initialApolloState,
}: IRenderStatic) => `
  <!doctype html>
  <html lang="en">
    <head>
      <title>Isomorphic app</title>
      <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta charSet='utf-8' />
      <meta name="viewport" content="width=device-width,initial-scale=1">
      <meta name="mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="twitter:card" content="summary">
      <meta name="twitter:site" content="@NikkitaFTW">
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-37411302-9"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){ dataLayer.push(arguments); }
        gtag('js', new Date());
        gtag('config', 'UA-37411302-9');
      </script>
      ${
        assets.client.css
          ? `<link rel="stylesheet" href="${assets.client.css}">`
          : ''
      }
      ${
        process.env.NODE_ENV === 'production'
          ? `<script src="${assets.client.js}" defer></script>`
          : `<script src="${assets.client.js}" defer crossorigin></script>`
      }
      <!-- Render the style tags gathered from the components into the DOM -->
      ${styleTags}
    </head>
    <body>
      <div id="root">${markup}</div>
      <script>
        window.__APOLLO_STATE__ = ${JSON.stringify(initialApolloState).replace(
          /</g,
          '\\u003c',
        )}
      </script>
    </body>
  </html>`
