import cookieParser from 'cookie-parser'
import express from 'express'
import 'isomorphic-fetch'
import React from 'react'
import { getDataFromTree } from 'react-apollo'
import { renderToString } from 'react-dom/server'
import { ServerStyleSheet } from 'styled-components'

import { client } from '@/apollo'
import { theme } from '@/theme'
import { renderStatic } from './renderStatic'
import { Root } from './Root'

const app = express()
const context = {}

app.use(cookieParser())

app.get('/*', async (req, res) => {
  const sheet = new ServerStyleSheet()
  const props = { client, theme, context, location: req.url }
  const root = () => <Root {...props} />

  try {
    await getDataFromTree(root())
  } catch (e) {
    console.log(e)
  }
  const initialApolloState = client.extract()

  const markup = renderToString(sheet.collectStyles(root()))

  const styleTags = sheet.getStyleTags()

  res.send(renderStatic({ assets, styleTags, markup, initialApolloState }))
})

export default app
