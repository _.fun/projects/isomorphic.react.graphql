interface ITheme {
  color: string
}

export const theme: ITheme = {
  color: 'white',
}
