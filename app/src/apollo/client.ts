import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'

import { config } from '@config'

const cache = () =>
  process.browser
    ? new InMemoryCache().restore(window.__APOLLO_STATE__)
    : new InMemoryCache()
const link = new HttpLink({
  uri: config.graphqlUri,
})

export const client = new ApolloClient({
  ssrMode: !process.browser,
  link,
  cache: cache(),
})
