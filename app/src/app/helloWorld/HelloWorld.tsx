import { useQuery } from '@apollo/react-hooks'
import React, { FunctionComponent } from 'react'

import { Title } from '@/components/typography'

import { getHello } from './graphql'

interface IHelloQuery {
  hello: string
}

export const HelloWorld: FunctionComponent = () => {
  const { data, loading, error } = useQuery<IHelloQuery>(getHello)
  if (loading) return <p>Loading</p>
  if (error) return <p>ERROR</p>

  return <Title>Hello {data.hello}</Title>
}
