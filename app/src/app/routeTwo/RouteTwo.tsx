import React, { FunctionComponent } from 'react'

import { Title } from '@/components/typography'

export const RouteTwo: FunctionComponent = () => <Title>Hello Two</Title>
