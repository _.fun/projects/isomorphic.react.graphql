const { dev, prod } = require('./webpack')

module.exports = (_, argv) => {
  if (argv.mode === 'production') return prod

  return dev
};
