import { IResolvers } from 'graphql-tools'

// A map of functions which return data for the schema.
export const resolvers: IResolvers = {
  Query: {
    hello: () => 'world'
  }
}
